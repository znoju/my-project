import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame {

    final static boolean shouldFill = true;
    final static boolean RIGHT_TO_LEFT = false;
    public static int memory;
    public static int value;
    public static int operation;

    static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100,100);

        //Set up the content pane.
        addComponents(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void addComponents(Container pane){
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }

        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        if (shouldFill) {
            //natural height, maximum width
            c.fill = GridBagConstraints.HORIZONTAL;
        }

        JTextField resultViewer = new JTextField();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 30;
        c.ipadx = 100;
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 0;
        pane.add(resultViewer, c);


        JButton button7 = new JButton("7");
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=7;
                }else{
                value=10*value+7;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 1;
        c.ipady = 10;
        c.ipadx = 1;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 1;
        pane.add(button7, c);

        JButton button8 = new JButton("8");
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=8;
                }else{
                    value=10*value+8;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 1;
        pane.add(button8, c);

        JButton button9 = new JButton("9");
        button9.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=9;
                }else{
                    value=10*value+9;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 1;
        pane.add(button9, c);

        JButton button4 = new JButton("4");
        button4.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=4;
                }else{
                    value=10*value+4;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 2;
        pane.add(button4, c);

        JButton button5 = new JButton("5");
        button5.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=5;
                }else{
                    value=10*value+5;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 2;
        pane.add(button5, c);

        JButton button6 = new JButton("6");
        button6.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=6;
                }else{
                    value=10*value+6;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 2;
        pane.add(button6, c);

        JButton button1 = new JButton("1");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=1;
                }else{
                    value=10*value+1;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        pane.add(button1, c);

        JButton button2 = new JButton("2");
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=2;
                }else{
                    value=10*value+2;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 3;
        pane.add(button2, c);

        JButton button3 = new JButton("3");
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(value==0){
                    value=3;
                }else{
                    value=10*value+3;}
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 3;
        pane.add(button3, c);

        JButton buttonPlus = new JButton("+");
        buttonPlus.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                memory = value;
                value = 0;
                operation = 0;
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 4;
        pane.add(buttonPlus, c);

        JButton buttonMinus = new JButton("-");
        buttonMinus.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                memory = value;
                value = 0;
                operation = 1;
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 4;
        pane.add(buttonMinus, c);

        JButton buttonDivide = new JButton("/");
        buttonDivide.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                memory = value;
                value = 0;
                operation = 2;
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 4;
        pane.add(buttonDivide, c);

        JButton buttonMultiply = new JButton("*");
        buttonMultiply.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                memory = value;
                value = 0;
                operation = 3;
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 5;
        pane.add(buttonMultiply, c);

        JButton buttonEquals = new JButton("=");
        buttonEquals.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(operation==0){
                    memory = memory + value;
                    value = memory;
                    resultViewer.setText(Integer.toString(value));
                }else if(operation==1){
                    memory = memory - value;
                    value = memory;
                    resultViewer.setText(Integer.toString(value));
                }else if(operation==2){
                    memory = memory / value;
                    value = memory;
                    resultViewer.setText(Integer.toString(value));
                }else if(operation==3){
                    memory = memory * value;
                    value = memory;
                    resultViewer.setText(Integer.toString(value));
                }
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 5;
        pane.add(buttonEquals, c);

        JButton buttonC = new JButton("C");
        buttonC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                value = 0;
                memory = value;
                resultViewer.setText(Integer.toString(value));
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 5;
        pane.add(buttonC, c);
    }
}
